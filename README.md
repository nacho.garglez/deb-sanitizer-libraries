# Common libraries for clang's sanitizers

The purpose of this repo is to maintain specific versions of common libraries, patched to be used with clang's sanitizers.

The idea is to built "sanitized" .deb packages that are installed into `/opt` (for example `/opt/tsan`).

This way, these libraries can be installed and uninstalled without affecting your other installation.

## Why do I need this?

* Sanitizers **do requiere** your library built with them to work properly.
* Upstream sources need little patches to build with `clang-9` and the sanitizers.
* A `.deb` package simplifies having multiple versions in your system.
* A `.deb` package eases the installation in your CI docker for automated testing.

## Pre-requesites

You need to set up your Debian/Ubuntu to build packages with:

```sh
sudo apt-get install build-essential fakeroot devscripts
```

## How to build and install

Into each library of your interest:

```sh
dpkg-buildpackage -b -uc -us
```

this should create a .deb file in the root directory, ready to install with:

```sh
sudo apt install ./library_xx.xx.platform.deb
```

## How to use

Prepend your `PKG_CONFIG_PATH` with the path of the library you are interested in, and set the env variable before invoking your build script.

Your project has to be built with `clang-9` with the corresponding sanitizer flags.

For example, for a meson project:

```sh
PKG_CONFIG_PATH=/opt/tsan/lib/x86_64-linux-gnu/pkgconfig/:$PKG_CONFIG_PATH \
CC=clang-9 \
meson tsan -Dc_args='-fsanitize=thread'
```

### False positives

The libraries have been tested to *build and work* with `clang-9` and the corresponding sanitizers, but may contain bugs or false positives.

A file with known suppressions can be found at `share` (for example: `/opt/.../share/.../tsan/glib.supp`) but you may need to update it.

Example (env variable needs to be set before running your executable):

```bash
export TSAN_OPTIONS="suppressions=/opt/tsan/share/glib-2.0/tsan/glib.supp" 
```